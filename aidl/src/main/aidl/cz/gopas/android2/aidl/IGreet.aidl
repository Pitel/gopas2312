package cz.gopas.android2.aidl;

interface IGreet {
    String greet(String name);
}