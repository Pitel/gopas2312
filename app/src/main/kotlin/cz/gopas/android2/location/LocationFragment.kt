package cz.gopas.android2.location

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber

@SuppressLint("MissingPermission")
class LocationFragment : BaseFragment() {
    private val locationListener = LocationListener { loc ->
        text.text = "$loc"
    }
    private val client by lazy {
        LocationServices.getFusedLocationProviderClient(requireActivity())
    }
    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            Timber.d("$isGranted")
            if (isGranted) {
                viewLifecycleOwner.lifecycleScope.launch {
                    text.text = "${client.lastLocation.await()}"
                }
                client.requestLocationUpdates(
                    LocationRequest.Builder(500).build(),
                    locationListener,
                    null
                )
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestPermissionLauncher.launch(
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    override fun onDestroyView() {
        client.removeLocationUpdates(locationListener)
        super.onDestroyView()
    }
}