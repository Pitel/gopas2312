package cz.gopas.android2.maps

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import cz.gopas.android2.R

class MapsFragment: Fragment(R.layout.fragment_maps) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
            .getMapAsync { map ->
                map.addMarker(
                    MarkerOptions().position(LatLng(49.0, 17.0))
                )
            }
    }
}