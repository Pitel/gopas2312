package cz.gopas.android2.coroutines

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class CoroutinesFragment : BaseFragment() {
    private val result = lifecycleScope.async(
        Dispatchers.Default,
        CoroutineStart.LAZY
    ) {
        foo()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                text.text = threadName + result.await() + result.await()
            }
        }
    }

    suspend fun heavyMath() = 2 + 2

    suspend fun foo() = heavyMath()
}