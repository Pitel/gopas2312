package cz.gopas.android2.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.RemoteViews
import cz.gopas.android2.R
import timber.log.Timber

class Widget : AppWidgetProvider() {
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        Timber.d("onUpdate ${appWidgetIds.asList()}")
        appWidgetIds.forEach { appWidgetId ->
            appWidgetManager.updateAppWidget(
                appWidgetId,
                RemoteViews(context.packageName, R.layout.widget).apply {
                    setOnClickPendingIntent(
                        R.id.widget_button,
                        PendingIntent.getBroadcast(
                            context,
                            REQUEST_CODE,
                            Intent(context, Widget::class.java).setAction(ACTION),
                            PendingIntent.FLAG_IMMUTABLE
                        )
                    )
                }
            )
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        Log.wtf(TAG, "Receive ${intent.action}")
        super.onReceive(context, intent)
        if (intent.action == ACTION) {
            CLICKS++
            AppWidgetManager.getInstance(context).updateAppWidget(
                ComponentName(context, Widget::class.java),
                RemoteViews(context.packageName, R.layout.widget).apply {
                    setTextViewText(R.id.widget_button, "$CLICKS clicks")
                }
            )
        }
    }

    private companion object {
        private const val REQUEST_CODE = 87
        private const val ACTION = "inc"
        private val TAG = Widget::class.simpleName
        private var CLICKS = 0
    }
}