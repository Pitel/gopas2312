package cz.gopas.android2.sensor

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.View
import androidx.core.content.getSystemService
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import kotlin.time.Duration.Companion.seconds

class SensorsFragment : BaseFragment() {
    private val sensorFlow = callbackFlow<Float> {
        val sensorManager = requireContext().getSystemService<SensorManager>()

        val sensorEventListener = object : SensorEventListener {
            override fun onSensorChanged(event: SensorEvent) {
                trySend(event.values.first())
            }

            override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
                Timber.d("Accuracy of $sensor changed to $accuracy")
            }
        }

        sensorManager?.getDefaultSensor(Sensor.TYPE_GRAVITY)?.let { sensor ->
            sensorManager.registerListener(
                sensorEventListener,
                sensor,
                SensorManager.SENSOR_DELAY_UI
            )
        }

        awaitClose {
            Timber.d("Closing sensor")
            sensorManager?.unregisterListener(sensorEventListener)
        }
    }.distinctUntilChanged().conflate()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sensorFlow.onEach {
            text.text = "$it"
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }
}