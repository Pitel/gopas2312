package cz.gopas.android2

import android.app.Application
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationManagerCompat
import com.google.android.material.color.DynamicColors
import com.google.firebase.Firebase
import com.google.firebase.messaging.messaging
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class App : Application() {
	init {
	    Timber.plant(Timber.DebugTree())
	}

	override fun onCreate() {
		super.onCreate()
		DynamicColors.applyToActivitiesIfAvailable(this)

		GlobalScope.launch {
			Timber.d("FCM token: ${Firebase.messaging.token.await()}")
		}

		NotificationManagerCompat.from(this).createNotificationChannel(
			NotificationChannelCompat.Builder(
				NOTIFICATION_CHANNEL_ID,
				NotificationManagerCompat.IMPORTANCE_DEFAULT
			).setName("Gopas channel").build()
		)
	}

	companion object {
		const val NOTIFICATION_CHANNEL_ID = "gopas"
	}
}