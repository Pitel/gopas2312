package cz.gopas.android2.push

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class FirebasePushService : FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Timber.d("New message ${message.data}")
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Timber.d("New token: $token")
    }
}