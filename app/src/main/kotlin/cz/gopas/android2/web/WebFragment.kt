package cz.gopas.android2.web

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.widget.Toast
import cz.gopas.android2.BaseFragment

class WebFragment: BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = WebView(requireContext())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as WebView).apply {
            settings.javaScriptEnabled = true
            loadUrl("file:///android_asset/index.html")
            addJavascriptInterface(JsIface(), "Android")
        }
    }

    private inner class JsIface {
        @JavascriptInterface
        fun showToast(text: String) {
            Toast.makeText(
                requireContext(),
                text,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}