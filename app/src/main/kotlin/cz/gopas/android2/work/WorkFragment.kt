package cz.gopas.android2.work

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.lifecycleScope
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.Operation
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.minutes
import kotlin.time.toJavaDuration

class WorkFragment: BaseFragment() {
    private val workRequest = PeriodicWorkRequestBuilder<GopasWorker>(
        15.minutes.toJavaDuration()
    )//.setConstraints(Constraints())
        .build()

    private val workManager by lazy { WorkManager.getInstance(requireContext()) }
    lateinit var workState: Flow<Operation.State>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        workState = workManager.enqueueUniquePeriodicWork(
            UNIQUE_WORK_ID,
            ExistingPeriodicWorkPolicy.KEEP,
            workRequest
        ).state.asFlow()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        workManager.getWorkInfosForUniqueWorkFlow(UNIQUE_WORK_ID).onEach {
            text.text = "$it"
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onDetach() {
        workManager.cancelUniqueWork(UNIQUE_WORK_ID)
        super.onDetach()
    }

    private companion object {
        const val UNIQUE_WORK_ID = "gopas"
    }
}