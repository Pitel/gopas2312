package cz.gopas.android2.http

import retrofit2.http.GET

interface HttpService {
    @GET("/ip")
    suspend fun ip(): IpModel
}