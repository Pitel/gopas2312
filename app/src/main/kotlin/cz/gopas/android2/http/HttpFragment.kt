package cz.gopas.android2.http

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import coil.load
import com.google.android.material.textview.MaterialTextView
import cz.gopas.android2.BaseFragment
import cz.gopas.android2.R
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HttpFragment : Fragment(R.layout.fragment_http) {
    private val okHttpClient = OkHttpClient.Builder()
        .addNetworkInterceptor(
            HttpLoggingInterceptor().setLevel(
                HttpLoggingInterceptor.Level.BODY
            )
        )
        .build()
    private val rest = Retrofit.Builder()
        .baseUrl("https://httpbin.org")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
        .create(HttpService::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            view.findViewById<MaterialTextView>(R.id.text).text = rest.ip().origin
        }
        view.findViewById<ImageView>(R.id.image).load("https://placekitten.com/1000/1000")
    }
}