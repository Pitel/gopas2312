package cz.gopas.android2.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import cz.gopas.android2.BaseFragment
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import cz.gopas.android2.aidl.IGreet
import timber.log.Timber
import java.lang.Exception

class ServiceFragment : BaseFragment() {

    private val service = MutableStateFlow<GopasService?>(null)
    private val aidlService = MutableStateFlow<IGreet?>(null)

    private val connection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            this@ServiceFragment.service.value = (service as GopasService.GopasBinder).service
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Timber.w("Disconnected")
        }
    }

    private val aidlConnection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            aidlService.value = IGreet.Stub.asInterface(service)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Timber.w("Disconnected")
        }
    }

    private val serviceIntent by lazy {
        Intent(requireContext(), GopasService::class.java)
    }

    override fun onStart() {
        super.onStart()
//        service.filterNotNull().onEach {
//            text.text = it.hello()
//        }.launchIn(viewLifecycleOwner.lifecycleScope)

        combine(
            service.map { it?.hello() },
            aidlService.map { it?.greet("Gopas") }
        ) { bound, aidl ->
            text.text = "$bound\n$aidl"
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        requireContext().bindService(
            serviceIntent,
            connection,
            Context.BIND_AUTO_CREATE
        )
        requireContext().bindService(
            Intent("GopasGreet").setPackage("cz.gopas.android2.aidl"),
            aidlConnection,
            Context.BIND_AUTO_CREATE
        )
        ContextCompat.startForegroundService(
            requireContext(),
            serviceIntent
        )
    }

    override fun onStop() {
        try {
            requireContext().unbindService(connection)
        } catch (e: Exception) {
            Timber.w(e)
        }
        try {
            requireContext().unbindService(aidlConnection)
        } catch (e: Exception) {
            Timber.w(e)
        }
        requireContext().stopService(serviceIntent)
        super.onStop()
    }
}