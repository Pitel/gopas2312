package cz.gopas.android2.service

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Binder
import android.util.Log
import androidx.core.app.NotificationCompat
import cz.gopas.android2.App
import timber.log.Timber

class GopasService: Service() {
    private val batteryReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Timber.d("Received ${intent.action}")
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            val charging = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
            Timber.d("${100 * level / scale.toFloat()} %, $charging")
        }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.d("Created")

        registerReceiver(
            batteryReceiver,
            IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("Start")
        startForeground(
            54,
            NotificationCompat.Builder(this, App.NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle("Gopas")
                .setContentText("Foreground service")
                .build()
        )
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?) = GopasBinder()

    fun hello() = "Hello from $this"

    override fun onDestroy() {
        Timber.d("Destroyed")
        unregisterReceiver(batteryReceiver)
        super.onDestroy()
    }

    inner class GopasBinder: Binder() {
        val service = this@GopasService
    }
}